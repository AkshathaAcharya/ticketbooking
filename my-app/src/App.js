
import { Header } from "./components/Header";
import { Footer } from "./components/Footer";
import DetailOfPlane from "./components/DetailsOfPlane"
import { BrowserRouter, Redirect, Route, Switch } from "react-router-dom"
import Home from "./components/Home"
import './App.css';
import Bookseat from "./components/BookSeat";
import Payment from "./components/payment";
import Booked from "./components/booked";
import ViewDetail from "./components/viewDetails";

function App() {
  return (
    <div className="App">
     
      <BrowserRouter>
         <header className="App-header">
       <Header/>
      </header>
     
           <main className="App-Content">
        <Switch>
            <Route path="/home" exact component={() => {
                            return <Redirect to="/"></Redirect>;
                            }} />
            <Route path="/" exact component={Home} />
            <Route path="/wayDetails" component={DetailOfPlane} />
            <Route path="/bookSeat" component={Bookseat} />
            <Route path="/payment" component={Payment} />
            <Route path="/booking" component={Booked} />
              <Route path="/viewDetail" component={ViewDetail} />
            <Redirect to="/"/>
          </Switch>
        </main>
         <footer className="App-footer">
          <Footer/>
         </footer>
        </BrowserRouter>
     
     
    </div>
  );
}

export default App;
