const intialStore = {
    totalTickets:[]
}

const resultReducer = (state = intialStore, action) => {
    const newState = { ...state };
    if (action.type === "RESULT") {
        console.log(action.payload)
        
        return {
            ...newState,
           totalTickets:[...newState.totalTickets,action.payload]
        }
    }
    return newState
}

export default resultReducer