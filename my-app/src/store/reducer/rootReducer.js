import { combineReducers } from "redux";
import detailsReducer from "./detailsreducer";
import passangerReducer from "./passengerreducer";
import seatDataReducer from "./seatDatareducer";
import selectCityReducer from "./selectCityReducer";
import Ticketreducer from "./ticketsReducer";
import resultReducer from "./resultreducer";

const rootreducer = combineReducers(
    [selectCityReducer,detailsReducer,seatDataReducer,passangerReducer,Ticketreducer,resultReducer]
)
export default rootreducer