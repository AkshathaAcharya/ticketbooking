const intialStore = {
    passangerArray:[]
}

const passangerReducer = (state = intialStore, action) => {
    const newState = { ...state };
    if (action.type === "ADD_PASSENGER") {
        console.log(action.payload)
        
        return {
            ...newState,
           passangerArray:action.payload
        }
    }
    return newState
}

export default passangerReducer