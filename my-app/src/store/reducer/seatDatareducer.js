const intialStore = {
    contentOfSeat: [],
    indexOfbooked:[],
    selectPlainId:"",
    
}

const seatDataReducer = (state = intialStore, action) => {
    const newState = { ...state };
    if (action.type === "SELECT_SEAT") {
        
        return {
            ...newState,
           contentOfSeat:action.payload,
           indexOfbooked:newState.indexOfbooked,
           selectPlainId:newState.selectPlainId
        }
    }
      if (action.type === "PROCEED") {
        console.log(action.payload)
        
        return {
            ...newState,
            indexOfbooked: action.payload,
            contentOfSeat:newState.contentOfSeat,
            selectPlainId:newState.selectPlainId
           
        }
    }
     if (action.type === "SELECT_PLAIN") {
        console.log(action.payload)
        
        return {
            ...newState,
            indexOfbooked:newState.indexOfbooked ,
            contentOfSeat:newState.contentOfSeat,
            selectPlainId:action.payload
           
        }
    }
    return newState
}

export default seatDataReducer