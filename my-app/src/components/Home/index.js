import React, { useState ,useEffect} from "react"
import dateIcon from "../../assets/icons/schedule.png"
import cityIcon from "../../assets/icons/buildings.png"
import "./style.scss"
import { useDispatch, useSelector } from "react-redux"
import Button from "../Button"
const Home = props => {
     const cityArray = useSelector((state)=>state[0].selectedCity)
    const [toCity, setToCity] = useState(cityArray.length>0?cityArray[0].toCity:"To city")
    const [fromCity, setFromCity] = useState(cityArray.length>0?cityArray[0].from:"From city")
    const [dateSelected, setDate] = useState(cityArray.length>0?cityArray[0].date:"");
    const [errmsg, setErrmsg] = useState(false)
    const [messgaes, setMessages] = useState("Enter all fields");

   
    const dispatch = useDispatch()
    useEffect(() => {setTimeout(() => {
        setErrmsg(false)
    }, 3000);},[errmsg])

    const submitForm = (e) => {
        console.log(toCity, fromCity, dateSelected.length)
        if (fromCity === "none" || dateSelected.length ===0) {
            setErrmsg(true)
            setMessages("Enter all fields")
        } else {
             var dateOne = new Date(dateSelected);  
           var dateTwo = new Date();  
           if (dateOne < dateTwo) {    
                setErrmsg(true)
                setMessages("Enter greater Date ") 
           } else {
               dispatch({type:"ADDCITY", payload:[{from:fromCity,toCity:toCity,date:dateSelected}]})
               props.history.push({	
            pathname: "/wayDetails",	
            state: {	
                redirect: "/wayDetails"
            },	
          })
               
            }   
        }
    }
 console.log(cityArray)
    return (
        <div className="homeWrap">  
        <div className="wrapOfFields">
                <div className="headEnd">
                    <h1>Journey for Dreams</h1>
                </div>
                <span className="spanOfFields">
                    <img src={cityIcon} className="iconSize" />
                    <select className="inputText" value={fromCity} onChange={(e) => {
                                setFromCity(e.target.value);
                                (e.target.value === "San Francisco") ?
                                    setToCity("Bangalore"):
                                (e.target.value === "Bangalore") ?
                                    setToCity("San Francisco"):
                                (e.target.value === "none") ?
                                setToCity("To city"): setToCity("To city")
                
                                }}>
                                    <option value="none">
                                        Select From city
                                    </option>
                                    <option value="Bangalore">
                                        Bangalore (BLR)
                                    </option>
                                    <option value="San Francisco">
                                        San Francisco (SFO)
                                    </option>
                                    </select>
                </span>

                <span className="spanOfFields">
                    <img src={cityIcon} className="iconSize" />
                    <input type="text" className="inputText" value={toCity} />
                </span>
                <span className="spanOfFields">
                    <img src={dateIcon} className="iconSize" />
                    <input type="date" value={dateSelected}className="inputText" onChange={(e) => {
                            setDate(e.target.value)
                        }}/>
                </span>
                <span className="spanOfFields">
             
                </span>
                <span className="spanOfFields">
                  
                    {errmsg ? <p className="errMessage">{messgaes}</p> :
                        <Button content="Search" onclickEvent={(e) => submitForm(e)}></Button>
                       }
                </span>
             
            </div>
   
       
            <div className="backGroundImage">
            
            </div>
       
    </div>)
}

export  default Home