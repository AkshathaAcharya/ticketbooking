import Button from "../Button"
import "./style.scss"
const Card = (props) => {
    return (<div className="cardWrap">
       <div><span className="unitSpan"><h4>{props.planeName}</h4></span></div>  
        <div  className="cardFirstLine">
           
            <span className="unitSpan"><span className="ticketHead">Boarding Time:</span> {props.BoardingTime}</span>
           
            <span className="unitSpan"><span className="ticketHead">Droping Point:</span> {props.DropingPoint}</span>
            <span className="unitSpan"><span className="ticketHead">Droping Time: </span> {props.DropingTime}</span>
     

            <span className="unitSpan"><span className="ticketHead">Ticket Price:</span>{props.TicketPrice}</span>
            <span className="unitSpan"><span className="ticketHead">Available Seats:</span>{props.availableSeats}</span>
            <span className="unitSpan"><span className="ticketHead">Total Journey Time:</span>{ props.journeyTime}</span>
            <span className="unitSpan"><span className="ticketHead">Ratings:</span>{props.ratings}</span>
            <span className="unitSpan"><span className="ticketHead">Total Seats Available:</span>{props.totalSeats}</span>
            <span className="unitSpan">
                <Button content={props.content} onclickEvent={props.onclickOfView} id={props.planeId}/>
          </span> 
                        </div>
    </div>)
}

export default Card