import ImageIcon  from "../../assets/icons/airplane.png"
import "./style.scss"

const Header = props => {
    return (
        <div className="headWrap" onClick={(e) => {     
           window.location.assign("/")	    
        }}>
            <span>
                <img src={ImageIcon} className="iconImage"></img>
            </span>
            <span className="headText">
                <h3>
                    bookFligts
                </h3>   
            </span>
        </div>)
}

export {Header}