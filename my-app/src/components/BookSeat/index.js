import Button from "../Button"
import avail from "../../assets/icons/avail.png"
import unavail from "../../assets/icons/unavail.png"
import book from "../../assets/icons/sofa.png"
import { useEffect, useState } from "react"
import {useSelector,useDispatch} from "react-redux"
import seatDataCities  from "../../constants/seatData"
import "./style.scss"

const Bookseat = (props) => {

 
    const dispatch = useDispatch()
    const [booked, setBooked] = useState(false);
   
    const cityArray = useSelector((state => state[0].selectedCity));
    const detailsOfCity = useSelector((state) => state[1].contentOFdata)
      const [arrayOfBooked, setArrayOfBooked] = useState([])
    const detailsOfSeat = useSelector((state) => state[2].contentOfSeat);
    const indexOfBookedArray = useSelector((state) => state[2].indexOfbooked);
    const [planeIdstate, setplaneIdstate] = useState("")
      const [errmsg, setErrmsg] = useState(false)
   
    useEffect(() => {  
        setplaneIdstate(parseInt(props.location.state.planeId) - 1)
        console.log(indexOfBookedArray)
        if (indexOfBookedArray.length > 0) {
            setArrayOfBooked([...indexOfBookedArray])
        }
         dispatch({type:"SELECT_SEAT", payload:seatDataCities[parseInt(props.location.state.planeId) - 1]})
         dispatch({type:"SELECT_PLAIN", payload:props.location.state.planeId})
          
    }, [])
   
   
  
    const proceedToPay = (e) => {
        if (arrayOfBooked.length === 0) {
            setErrmsg(true)
        } else {
            dispatch({ type: "PROCEED", payload: arrayOfBooked });
            props.history.push({
                pathname: "/payment",
                state: {
                    redirect: "/payment",
                    
                },
            })
        }
    }
   useEffect(() => {setTimeout(() => {
        setErrmsg(false)
   }, 3000);
   }, [errmsg])
    
    return (<div className="bookSeatOfPlaneWrap">
         {cityArray.map((item, index) => {
            return(<div key={index} className="itemOfCity">
                <span className="itemInCity" > {item.from} </span>
                <span className="itemInCity" > 
                 to </span>
                <span className="itemInCity" >   {item.toCity} </span>

                 <span className="itemInCity" > on </span>
                <span className="itemInCity" >  {item.date} </span>
                
            </div>)
            
        })}
        <div className="colorOfSeat">
            <div className="headSpan">
            <span className="headUnit"><img src={avail} className="iconStyle"/>   Available</span>
              <span className="headUnit"><img src={unavail} className="iconStyle"/>   Unavailable</span>
           </div>
           <div className="headSpan">
           Select available Seat
           </div>
            <div className="allignSeat">
                {detailsOfCity.length!==0 && Object.values(detailsOfSeat).map((item, index) => {
                    return (
                        <div key={index} >
                        {item === "available" && <span className="headUnit" onClick={(e) => {
                            if (arrayOfBooked.includes(index)) {
                                var splicedArray =arrayOfBooked.splice(arrayOfBooked.indexOf(index),1)
                               setArrayOfBooked([...arrayOfBooked])
                            } else {
                                setArrayOfBooked([...arrayOfBooked, index]);
                               
                            }
                            }}
                            >
                            {arrayOfBooked.includes(index) ? <img src={book} className="iconStyle availClick" /> : <img src={avail} className="iconStyle availClick" />}</span>}
                          {item === "Unavailable" &&  <span className="headUnit"><img src={unavail} className="iconStyle"/></span> }
                        </div>
                    )

               })}
              </div>
           
        </div>
        <span className="buttonGroup">
            {errmsg ? <p className="errMessage">Select atleast one seat</p> : <Button content="proceed to book" onclickEvent={proceedToPay}></Button>
            }
        <Button content="back" onclickEvent={(e) => props.history.goBack()}></Button>
        </span>

        
    </div>)
}

export default Bookseat

