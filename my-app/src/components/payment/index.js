import Button from "../Button"
import avail from "../../assets/icons/avail.png"
import unavail from "../../assets/icons/unavail.png"
import book from "../../assets/icons/sofa.png"
import { useEffect, useState } from "react"
import {useSelector,useDispatch} from "react-redux"
import seatDataCities  from "../../constants/seatData"
import "./style.scss"

const Payment = (props) => {

 
     const dispatch = useDispatch()
    const [name, SetName] = useState("")
    const [gender, Setgender] = useState("")
    const [age, SetAge] = useState("")
    const [email, SetEmail] = useState("")
    const [phone, SetPhone] = useState("")
 
      const [cardNumber, SetCard] = useState("")
    const [NameonAccount, SetNameonAccount] = useState("")
    const [cvv, Setcvv] = useState("")

 const [showPay, SetshowPay] = useState(false)
      const [errmsg, setErrmsg] = useState(false)
      const indexOfBooked = useSelector((state) => state[2].indexOfbooked);
       const indexOfBookedArray = useSelector((state) => state[3].passangerArray);
   
    useEffect(() => {
        console.log(indexOfBookedArray)
        if (indexOfBookedArray.length !== 0) {
            SetName(indexOfBookedArray[0].name);
            Setgender(indexOfBookedArray[0].gender);
            SetAge(indexOfBookedArray[0].age);
            SetEmail(indexOfBookedArray[0].email);
            SetPhone(indexOfBookedArray[0].phone);
            SetCard(indexOfBookedArray[0].cardNumber);
            SetNameonAccount(indexOfBookedArray[0].NameonAccount);
            Setcvv(indexOfBookedArray[0].cvv)
           
        }
        if(indexOfBooked.length>0){
SetshowPay(true)
        }
    },[])
    const viewDetail=()=>{
          props.history.push({
                pathname: "/viewDetail",
                state: {
                    redirect: "/viewDetail",
                    
                },
            })
            
    }
     
    const payit = () => {
        if (name && gender && age && email && phone && cardNumber && cvv && NameonAccount) {

            dispatch({
                type: "ADD_PASSENGER", payload: [{
                    name: name, gender: gender, age: age, email: email, phone: phone,
                    cardNumber: cardNumber,
                    cvv: cvv,
                    NameonAccount:NameonAccount
                
            }]
            })
             props.history.push({
                pathname: "/booking",
                state: {
                    redirect: "/booking"
                },
            })
            
        } else {
            setErrmsg(true)
       }
    }

       useEffect(() => {setTimeout(() => {
        setErrmsg(false)
    }, 3000);},[errmsg])
    return (<div className="paymentOfPlaneWrap">
        
        <div>
            <h3 className="passhead">Passenger Details</h3>
            <div className="whiteBackground">
                <span className="headStyle"><span className="ticketHead">Name : </span>
                    <input type="text" className="inputText" value={name} onChange={(e) => {
                        SetName(e.target.value)
                    } }/></span>
                <span className="headStyle"><span className="ticketHead">Gender : </span>
                    <span  className="radioText" >
              <input type="radio" id="male" name="gender" value="male" checked={gender==="male"?true:false} onChange={(e) => {
                        Setgender(e.target.value)
                    } }/>
                    <label for="male">Male</label>
                    <input type="radio" id="female" name="gender" value="female"  checked={gender==="female"?true:false}  onChange={(e) => {
                        Setgender(e.target.value)
                    } }/>
                        <label for="female">Female</label>
                        </span>
                </span>
                 <span className="headStyle"><span className="ticketHead">Age :</span>
              <input type="text"  className="inputText" value={age} onChange={(e) => {
                        SetAge(e.target.value)
                    } }/>
                </span>
                 <span className="headStyle"><span className="ticketHead">Email :</span>
              <input type="email"  className="inputText" value={email} onChange={(e) => {
                        SetEmail(e.target.value)
                    } }/>
                </span>
                  <span className="headStyle"> <span className="ticketHead">Phone :</span>
              <input type="number" className="inputText" value={phone}  onChange={(e) => {
                        SetPhone(e.target.value)
                    } }/>
                   
                </span>
                <h4>payment details (Debit card)</h4>
              
                   <span className="headStyle"> <span className="ticketHead">Card Number :</span>
              <input type="text" className="inputText" value={cardNumber} onChange={(e) => {
                        SetCard(e.target.value)
                    } }/>
                   
                </span>
                   <span className="headStyle"> <span className="ticketHead">Account Holder Name :</span>
              <input type="text" className="inputText" value={NameonAccount} onChange={(e) => {
                        SetNameonAccount(e.target.value)
                    } }/>
                   
                </span>
                 
                   <span className="headStyle"> <span className="ticketHead">Cvv :</span>
              <input type="number" className="inputText" value={cvv} onChange={(e) => {
                        Setcvv(e.target.value)
                    } }/>
                   
                </span>
            </div>
     </div>
           <span className="buttonGroup passhead">
         
            {showPay && <div> {errmsg ? <p className="errMessage">Enter all fields</p> : <Button content="proceed to pay" onclickEvent={(e) => { payit(e) }}></Button>
            }</div>}
        <Button content="back" onclickEvent={(e) => props.history.goBack()}></Button>
        </span>

    </div>)
}

export default Payment

