import Button from "../Button"
import { useDispatch, useSelector } from "react-redux";
import datafromCities from "../../constants/cityData"
import "./style.scss"
import { useEffect, useState } from "react";
import Card from "../Card";

const DetailOfPlane = props => {
  const dispatch = useDispatch()
    const cityArray = useSelector((state => state[0].selectedCity));
    const detailsOfCity = useSelector((state)=>state[1].contentOFdata)
       const arrayOfData = useSelector((state)=>state[5].totalTickets)
    const [fromCityOfData, setfromCityOfData] = useState("")
    
    useEffect(() => {
        setfromCityOfData(cityArray[0]?.from)
        if (cityArray[0]?.from.includes("Bangalore")) {
            dispatch({type:"ADDDETAIL", payload:datafromCities[0]?.bangalore})
        }
        if (cityArray[0]?.from.includes("Francisco")) {
             dispatch({type:"ADDDETAIL", payload:datafromCities[0]?.Francisco})
        }
    }, [])
    const viewDetail =(e)=>{
          props.history.push({	
            pathname: "/viewDetail",	
            state: {	
                redirect: "/viewDetail",
                lengthOfArrayOfData :arrayOfData.length
                
            },	
          })
    }
    const viewSeat = (e) => {
           props.history.push({	
            pathname: "/bookSeat",	
            state: {	
                redirect: "/bookSeat",
                planeId:e.target.id
            },	
          })
    }
    const homepage=(e)=>{
           props.history.push({	
            pathname: "/",	
            state: {	
                redirect: "/",
                
            },	
          })
    }

    return (<div className="DetailOfPlaneWrap">
        {cityArray.map((item, index) => {
            return(<div key={index} className="itemOfCity">
                <span className="itemInCity" > {item.from} </span>
                <span className="itemInCity" > 
                 to </span>
                <span className="itemInCity" >   {item.toCity} </span>
                 <span className="itemInCity" > on </span>
                <span className="itemInCity" >  {item.date} </span>
                 <span className="itemInCity" >  <Button content="modify" onclickEvent={(e) => homepage(e)}> </Button></span>
            
          </div>)
            
        })}

       
      <span className="countOfPlane"> <h3>{detailsOfCity.length} Air Planes Found</h3></span> 
              
                {detailsOfCity.map((item, index) => {
                    console.log(item);
                    return (<div key={index} className="cardInDetail">
                        <Card planeName={item.planeName}
                            BoardingTime={item.BoardingTime}
                                DropingPoint ={item.DropingPoint}
                                DropingTime={item.DropingTime}
                                TicketPrice={item.TicketPrice}
                                availableSeats={item.availableSeats}
                                journeyTime={item.journeyTime}
                                ratings={item.ratings}
                            totalSeats={item.totalSeats}
                            content="view seats"
                            onclickOfView={(e)=>viewSeat(e)}
                            planeId={item.planeId}
                        />
                        
                    </div>)
                })}
            
                        
           <span className="buttonGroup passhead">
        <Button content="back" onclickEvent={(e) => homepage(e)}></Button>
        
  {arrayOfData.length>0 &&     <Button content="view details" onclickEvent={(e) => viewDetail(e)}></Button>
         }
        </span>
    </div>)
}

export default DetailOfPlane