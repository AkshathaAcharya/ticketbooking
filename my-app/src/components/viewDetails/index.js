import Button from "../Button"
import avail from "../../assets/icons/avail.png"
import unavail from "../../assets/icons/unavail.png"
import book from "../../assets/icons/sofa.png"
import { useEffect, useState } from "react"
import {useSelector,useDispatch} from "react-redux"
import seatDataCities  from "../../constants/seatData"
import "./style.scss"

const ViewDetail = (props) => {
const dispatch = useDispatch()
  const lengthOfArray = useSelector((state) => state[4].ticketsBooked);
  const arrayinData = useSelector((state) => state[5].totalTickets);
  
useEffect(()=>{
    if(!props.location.state?.lengthOfArrayOfData){
if(lengthOfArray!== undefined){
    


dispatch({type:"RESULT",payload:[...new Set([...arrayinData,lengthOfArray])]})
dispatch({type:"BOOKED_DETAIL",payload:[]})


}
    }
},[])
 console.log(arrayinData)

    return (<div className="ViewDetailOfPlaneWrap">
        <h2>Ticket Details</h2>
        {arrayinData.length>0 && arrayinData.map((item,index)=>{
      
            return(
            <div className="cardWrap" key={index}>
       <div><span className="unitSpan"><h4>{item[index][0].planeName}</h4></span>
       </div>  
        <div  className="cardFirstLine">
                       <span className="unitSpan"><span className="ticketHead">Name:</span>{item[index][0].name}</span>
            <span className="unitSpan"><span className="ticketHead">Age :</span>{item[index][0].age}</span>    
            <span className="unitSpan"><span className="ticketHead">Gender :</span>{item[index][0].gender}</span>
                <span className="unitSpan"><span className="ticketHead">Email :</span>{item[index][0].email}</span>
                    <span className="unitSpan"><span className="ticketHead">Phone :</span>{item[index][0].phone}</span>
    <span className="unitSpan"><span className="ticketHead">From:</span> {item[index][0].from}</span>
        <span className="unitSpan"><span className="ticketHead">To:</span> {item[index][0].toCity}</span>

    <span className="unitSpan"><span className="ticketHead">Date:</span> {item[index][0].date}</span>

            <span className="unitSpan"><span className="ticketHead">Boarding Time:</span> {item[index][0].BoardingTime}</span>
           
            <span className="unitSpan"><span className="ticketHead">Droping Point:</span> {item[index][0].DropingPoint}</span>
            <span className="unitSpan"><span className="ticketHead">Droping Time: </span> {item[index][0].DropingTime}</span>
     

            <span className="unitSpan"><span className="ticketHead">Ticket Price:</span>{item[index][0].TicketPrice}</span>
            <span className="unitSpan"><span className="ticketHead">Total Journey Time:</span>{ item[index][0].journeyTime}</span>

            <span className="unitSpan"><span className="ticketHead"> Seats booked:</span>
            {item[index][0].bookedseats.map((i,ind)=>{
  return(  <span id={ind}>{i},</span>)
            })}
            </span>
           
                        </div>
    </div>
    
            )
        })}
       
       <Button content="back" onclickEvent={(e) =>  props.history.push({
                pathname: "/wayDetails",
                state: {
                    redirect: "/wayDetails"
                },
            })}></Button> 
    </div>)
}

export default ViewDetail

