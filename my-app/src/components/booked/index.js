import Button from "../Button"
import avail from "../../assets/icons/avail.png"
import unavail from "../../assets/icons/unavail.png"
import book from "../../assets/icons/sofa.png"
import { useEffect, useState } from "react"
import {useSelector,useDispatch} from "react-redux"
import seatDataCities  from "../../constants/seatData"
import "./style.scss"

const Booked = (props) => {
   const dispatch = useDispatch()
    const cityArray = useSelector((state)=>state[0].selectedCity)
    const detailsOfCity = useSelector((state)=>state[1].contentOFdata)
    const detailsOfSeat = useSelector((state) => state[2].contentOfSeat);
    const indexOfBookedArray = useSelector((state) => state[2].indexOfbooked);
      const dataOfPlane = useSelector((state) => state[2].selectPlainId);
    const bookedarray = useSelector((state) => state[3].passangerArray);

    const [objectOfTicket,setobjectOfTicket] = useState([])
     
    const seatsBookedOnPlane = useSelector((state)=>state[1].seatsonplane)
       const flightDataArray = useSelector((state)=>state[1].flightData)
    const lengthOfArray = useSelector((state) => state[4].ticketsBooked);

  const viewDetail=()=>{
     const data = [ 
      {
            name:bookedarray[0]?.name,
             age:bookedarray[0]?.age,
              email:bookedarray[0]?.email,
               phone:bookedarray[0]?.phone,
                gender:bookedarray[0]?.gender,
      from :cityArray[0]?.from,
      toCity: cityArray[0]?.toCity, 
      date:   cityArray[0]?.date,
BoardingTime:flightDataArray[0]?.BoardingTime,
DropingPoint:flightDataArray[0]?.DropingPoint,
DropingTime:flightDataArray[0]?.DropingTime,
TicketPrice:flightDataArray[0]?.TicketPrice,
journeyTime:flightDataArray[0]?.journeyTime,
planeName:flightDataArray[0]?.planeName,
BoardingTime:flightDataArray[0]?.BoardingTime,
bookedseats : seatsBookedOnPlane
        }]


dispatch({type:"BOOKED_DETAIL",payload:[ ...data]})
          props.history.push({
                pathname: "/viewDetail",
                state: {
                    redirect: "/viewDetail",
                },
            })
            
    }
     console.log(cityArray,detailsOfCity,detailsOfSeat,indexOfBookedArray,bookedarray)
    useEffect(()=>{
     var bookedSeats =[];
    for(var i =0;i<indexOfBookedArray.length;i++){
    bookedSeats.push(`a${indexOfBookedArray[i]+1}`)
 
     detailsOfSeat[`a${indexOfBookedArray[i]+1}`] = "Unavailable";
    }
   var arrayOfPlane= detailsOfCity.filter((item,index)=>{
     return item.planeId == dataOfPlane
    })

    var arrayOfAvailable = Object.values(detailsOfSeat).filter((item,index)=>{
      return item ==="available"
    })

var contentOfPlane = detailsOfCity.forEach(element => {
  if(element.planeId=== arrayOfPlane[0].planeId){
    element.availableSeats = arrayOfAvailable.length
  }
});
  console.log(arrayOfPlane, arrayOfAvailable,detailsOfCity,bookedSeats)
  if(lengthOfArray!== undefined){
    setobjectOfTicket(prev=>[...prev])
  }

         dispatch({type:"SELECT_SEAT", payload:detailsOfSeat})
          dispatch({ type: "PROCEED", payload: [] });
           dispatch({ type: "SEAT_ON_PLANE", payload: bookedSeats });
             dispatch({ type: "FLIGHT_CONTENT", payload: arrayOfPlane });

    },[]);

 
  
    return (<div className="bookedOfPlaneWrap">
        
     <div className="successWrap">
            <h2 className="passhead">Payment Status</h2>
            <span className="successMessage">Successfully Booked Seat !!!</span>
          <span >       <Button content="view details" onclickEvent={(e)=>{viewDetail(e)}}></Button>   </span>

     </div>
    

    </div>)
}

export default Booked

