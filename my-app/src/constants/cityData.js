const datafromCities = [
    {
        bangalore: [
            {
            planeId:1,
            planeName: "AIRBUS A330-300",
            BoardingTime: "1:00 PM",
            DropingPoint: "San Francisco",
            journeyTime: "05h 10m",
            DropingTime: "6:00 PM",
            ratings: "3.0",
            TicketPrice: "INR 10000 /per",
            availableSeats: 10,
            totalSeats:12
            },
               {
            planeId:2,
            planeName: "AIRBUS A550-370",
            BoardingTime: "8:00 PM",
            DropingPoint: "San Francisco",
            journeyTime: "07h 30m",
            DropingTime: "4:00 AM",
            ratings: "4.0",
            TicketPrice: "INR 7000 /per",
            availableSeats: 8,
            totalSeats:12
            },
                {
            planeId:3,
            planeName: "AIRBUS A880-430",
            BoardingTime: "8:00 AM",
            DropingPoint: "San Francisco",
            journeyTime: "05h 30m",
            DropingTime: "2:00 PM",
            ratings: "5.0",
            TicketPrice: "INR 5000 /per",
            availableSeats: 3,
            totalSeats:12
            }
          
        ],
           Francisco: [
            {
            planeId:4,
            planeName: "BOEING 777-200",
            BoardingTime: "2:00 PM",
            DropingPoint: "Bangalore",
            journeyTime: "06h 15m",
            DropingTime: "9:00 PM",
            ratings: "2.0",
            TicketPrice: "INR 15000 /per",
            availableSeats: 9,
            totalSeats:12
            },
               {
            planeId:5,
            planeName: "BOEING 755-111",
            BoardingTime: "3:00 PM",
            DropingPoint: "Bangalore",
            journeyTime: "03h 30m",
            DropingTime: "7:00 PM",
            ratings: "4.0",
            TicketPrice: "INR 5000 /per",
            availableSeats: 2,
            totalSeats:12
            }
          
        ]
    }
]

export default datafromCities